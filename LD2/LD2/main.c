/*
 * GccApplication1.c
 *
 * Created: 2019-02-22 08:37:06
 * Author : 20163596
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#define F_CPU 16000000
#include <util/delay.h>

volatile uint8_t tot_overflow;

/*int main(void)
{
    DDRA=255;
	DDRC=255;
	TCCR1A = (0 << WGM10) | (0 << WGM11) | (1 << COM1A1) | (1 << COM1B1);
	TCCR1B = (1 << CS10) | (0 << WGM12)| (0 << WGM13);
	TCNT1H = 0;
	TCNT1L = 0;
    while (1) 
    {
		PORTA = TCNT1H;
		PORTC = TCNT1L;
		_delay_ms(250);
    }
} */
// PAPILDOMA PIRMAS
/*ISR(TIMER1_OVF_vect)
{
	tot_overflow++;
}

int main(void)
{
	TCCR1A = (1 << WGM10) | (0 << WGM11) | (1 << COM1A1) | (0 << COM1B1);
	TCCR1B = (1 << CS12) | (0 << CS11) |(0 << CS10) | (1 << WGM12)| (0 << WGM13);
	
	TCNT1 = 0;
	TIMSK |= (1 << TOIE1);
	DDRC = 255;
	
	tot_overflow = 0;
	sei();
		
	while(1)
	{
		if (tot_overflow>= 122)
		{
			if (TCNT1 >= 18)
			{
				PORTC ^= (1<<6);
				TCNT1 = 0;
				tot_overflow = 0;
			}
		}
	}
}*/
//PAPILDOMA ANTRAS
ISR(TIMER1_OVF_vect)
{
	tot_overflow++;
}
int main(void)
{
	TCCR1A = (1 << WGM10) | (0 << WGM11) | (1 << COM1A1) | (0 << COM1B1);
	TCCR1B = (1 << CS12) | (0 << CS11) |(1 << CS10) | (1 << WGM12)| (0 << WGM13);
	
	TCNT1 = 0;
	TIMSK |= (1 << TOIE1);
	DDRC = (1<<6);
	
	tot_overflow = 0;
	sei();
	int i = 0;
	
	while(1)
	{
		if (i<4){	
			if (tot_overflow>= 122)
				{
					if (TCNT1 >= 18)
					{
						PORTC ^= (1<<6);
						TCNT1 = 0;
						tot_overflow = 0;
						i++;
					}
				}
		}
		if (i>=4)
			if (tot_overflow>= 183)
			{
				if (TCNT1 >= 27)
				{
					PORTC ^= (1<<6);
					TCNT1 = 0;
					tot_overflow = 0;
					i++;
				}
				if (i==8){
					i=0;
				}
			}
	}
}