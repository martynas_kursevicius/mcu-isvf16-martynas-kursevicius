/*
 * VJ3.c
 *
 * Created: 2019-03-01 08:38:55
 * Author : 20163596
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#define F_CPU 16000000
#include <util/delay.h>

volatile unsigned char result;
ISR(ADC_vect){
	result = ADCH;
}

int main(void)
{
	DDRB = 255;
	ADMUX = 0b01100000; 
	ADCSRA = 0b10001111;
	DDRD=(1<<PD7);
	TCCR2=(1<<WGM21)|(1<<WGM20)|(1<<COM21)|(0<<COM20)|(1<<CS20);
	
	sei(); 
    while (1) 
    {
		for(int i=0; i<255; i++)
		{
			OCR2=i;		
			ADCSRA |= (1 << ADSC);
			PORTB = result;
			_delay_ms(20);	
		}
		for(int i=255; i>0; i--)
		{
			OCR2=i;
			ADCSRA |= (1 << ADSC);
			PORTB = result;
			_delay_ms(20);
		}
    }
}